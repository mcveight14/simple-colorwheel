# Simple-Colorwheel
## A minimalist colorwhell
Simple-Colorwheel is an uncomplicated colorwheel, make for developers and artists.

## Features

- Convert HEX to RGB(Developing).
- Get complementary colors(Developing).
- Get analogus colors(Developing).


# Requeriments



## Install tkinter
These depends on the distro that you use here are some examples.
### void-linux
```bash
xbps-install python3-tkinter
```
### Ubuntu and derivatives
```bash
apt-get install python3-tk
```
### Archlinux and derivatives
```bash
pacman -S tk
```
## Install requirements
```bash
pip install -r requirements.txt
```


## Development

Want to contribute? Great!
You can:
+ send us feedback.
+ give us suggestions.
+ contribute with code. 



